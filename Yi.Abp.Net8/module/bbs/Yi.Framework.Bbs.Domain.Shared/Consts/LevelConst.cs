﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Yi.Framework.Bbs.Domain.Shared.Consts
{
    public class LevelConst
    {
        public const string LevelCacheKey=nameof(LevelCacheKey);

        public const string Level_Low_Zero = "经验提升等级低于或等于0";
    }
}
